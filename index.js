const express = require("express");
const app = express();
const port = process.env.PORT || 4040;

app.use(express.json());

let users = [
    {
        name: 'John',
        age: 18,
        username : 'johnsmith99'
    },
    {
        name: 'Johnson',
        age: 18,
        username : 'johnson12'
    },
    {
        name: 'Smith',
        age: 18,
        username : 'smith123'
    }
]
app.get('/users', (req,res) => {
    return res.send(users);
})

let products = [
    {
        name: 'Keyboard',
        price: 300,
        isActive: true
    },
    {
        name: 'Monitor',
        price: 1500,
        isActive: true
    },
    {
        name: 'Mouse',
        price: 150,
        isActive: false
    }
]
app.get('/products', (req,res) => {
    return res.send(products);
})

app.post('/users', (req,res) => {
    if(!req.body.hasOwnProperty('name')){
        return res.status(400).send({
            error: "Bad Request - missing required parameter NAME"
        })
    }
    if(!req.body.hasOwnProperty('age')){
        return res.status(400).send({
            error: "Bad Request - missing required parameter AGE"
        })
    }
    if(!req.body.hasOwnProperty('username')){
        return res.status(400).send({
            error:"Bad Request - missing required parameter USERNAME"
        })
    }
})

app.listen(port,()=>console.log(`Server is running at port ${port}`));