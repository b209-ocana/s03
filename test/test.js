const chai = require('chai');
const { assert } = require('chai');

//import and use chai-http to allow chai to send requests to our server
const http = require('chai-http');
chai.use(http);

describe('api_test_suite_users', () => {
    it('test_api_get_users_is_running', (done) => {
        //.request() method is used from chai to create an http request to the given server
        //.get('/endpoint') is used to run/access a get method route
        //.end() which is used to access the response from the route. It has an anonymous function as an argument that receives 2 object, the err or the response object
        chai.request('http://localhost:4040')
            .get('/users')
            .end((err,res) => {
                //.isDefined() is an assertion that the give data is not undefined. It's like a shortcut to .notEqual(typeof data, undefined)
                assert.isDefined(res);
                //done() method is used to tell chai-http
                done();
            })
    })

    it('test_api_get_users_returns_array', (done) => {
        chai.request('http://localhost:4040')
            .get('/users')
            .end((err,res) => {
                //res.body contains the body of the response. The data send from res.send
                //.isArray is an assertion that the given data is an array
                assert.isArray(res.body);
                done();
            })
    })

    it('test_api_get_users_array_first_item_is_not_undefined', (done) => {
        chai.request('http://localhost:4040')
            .get('/users')
            .end((err,res) => {
                assert.notEqual(res.body[0],undefined);
                done();
            })
    })

    it('test_api_get_users_array_last_item_is_not_undefined', (done) => {
        chai.request('http://localhost:4040')
            .get('/users')
            .end((err,res) => {
                assert.notEqual(res.body[res.body.length - 1], undefined);
                done();
            })
    })

    it('test_api_post_users_is_running', (done) => {
        chai.request('http://localhost:4040')
            .post('/users')
            .end((err,res) => {
                assert.isDefined(res);
                done();
            })
    })

    it('test_api_post_users_returns_400_if_no_name', (done) =>{
        //.post() which is used by chai-http to access a post method
        //.type() which is used to tell chat that the request body is going to be stringified as json
        //.send() used to send the request body
        chai.request('http://localhost:4040')
            .post('/users')
            .type('json')
            .send({
                age:28,
                username:'irene91'
            })
            .end((err,res) => {
                assert.equal(res.status,400);
                done();
            })
    })
    it('test_api_post_users_returns_400_if_no_age', (done) => {
        chai.request('http://localhost:4040')
            .post('/users')
            .type('json')
            .send({
                name:'John Doe',
                username: 'johndoe11'
            })
            .end((err,res) => {
                assert.equal(res.status,400);
                done();
            })
    })
    it('test_api_post_users_returns_400_if_no_username', (done) => {
        chai.request('http://localhost:4040')
        .post('/users')
        .type('json')
        .send({
            name:'John Doe',
            age: 87
        })
        .end((err,res) => {
            assert.equal(res.status, 400);
            done();
        })
    })
})

describe('api_test_suite_products', () => {
    it('test_api_get_products_is_running', (done) => {
        chai.request('http://localhost:4040')
            .get('/products')
            .end((err, res) =>{
                assert.isDefined(res);
                done();
            })
    })

    it('test_api_get_products_returns_array', (done) => {
        chai.request('http://localhost:4040')
            .get('/products')
            .end((err,res) =>{
                assert.isArray(res.body);
                done();
            })
    })

    it('test_api_get_products_first_item_is_object', (done) => {
        chai.request('http://localhost:4040')
            .get('/products')
            .end((err,res) => {
                assert.isObject(res.body[0])
                done();
            })
    })
})